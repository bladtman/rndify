(require :cl-lex)
(require :yacc)

(defun loopcall (f)
  (multiple-value-bind (typ val) (funcall f)
    (format t "~s: ~s ~d~%" typ val (length val))
    (unless (null typ)
      (loopcall f))))

(cl-lex:define-string-lexer :mylex
  ("=" (return 'EQSIGN))
  ("\\(" (return 'LEFT-PAREN))
  ("\\)" (return 'RIGHT-PAREN))
  ("(\\\\ |\\\\\\)|\"|[a-zA-Z0-9!¤%&/?ÆØÅæøå])+" (return (values 'NAME (remove #\\ $@)))))

(let ((l  (:mylex "a = 10 \" a b cd \"")))
  (loopcall l))

(defun lex (expression)
  (let ((lexer (:mylex expression)))
    (loopcall lexer)))

(yacc:define-parser myparse
  (:start-symbol Character-Classes)
  (:terminals (EQSIGN NAME LEFT-PAREN RIGHT-PAREN))

  (Character-Classes
    (Character-Class #'identity)
    (Character-Class Character-Classes #'cons))

  (Character-Class
    (NAME EQSIGN LEFT-PAREN NameList RIGHT-PAREN #'(lambda (name eqsign left-paren names right-paren)
                              eqsign  ;ignore
                              left-paren
                              right-paren
                              (list name names))))

  (NameList
    (NAME #'list)
    (NAME NameList #'cons)))

(defun file-string (path)
  (with-open-file (stream path)
    (let ((data (make-string (file-length stream))))
      (read-sequence data stream)
      data)))

(defun parse-string (expression)
   (yacc:parse-with-lexer (:mylex expression) myparse))

(defun parse-file (path)
  (parse-string (file-string path)))
