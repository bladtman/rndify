(defstruct character-class (atoms '() :type list))

(defun shuffle-list (lst)
  (sort (copy-list lst) (lambda (x y)
                          x  ;ignore x
                          y  ;ignore y
                          (= 0 (random 2)))))

(defun first-unescaped (text &optional (index 0))
  (if (>= index (length text))
    NIL
    (let ((next-char (subseq text index (1+ index))))
      (cond ((string= "\\" next-char)
             (first-unescaped text (+ index 2)))
            ((string= " " next-char)
             index)
            (t
             (first-unescaped text (1+ index)))))))

(defun parse-atoms (atoms &optional (atom-list '()))
  (cond ((string= "" atoms)
         atom-list)
        ((string= " " (subseq atoms 0 1))
         (parse-atoms (subseq atoms 1 NIL) atom-list))
        (t
         (let* ((index (first-unescaped atoms))
                (symbl (remove #\\ (subseq atoms 0 index))))
           (if index
             (parse-atoms (subseq atoms (1+ index) NIL)
                          (cons symbl atom-list))
             (cons symbl atom-list))))))

(defun parse-class-definition (line dictionary)
  (let* ((index-= (search "=" line))
         (ident (string-trim " " (subseq line 0 index-=))))
    (unless index-=
      (error (concatenate 'string "could not parse line: " line)))
    (setf (gethash ident dictionary)
          (make-character-class :atoms (parse-atoms
                                         (subseq line
                                                 (1+ index-=)
                                                 NIL))))))

(defun make-node (class)
  (car (shuffle-list (character-class-atoms class))))

(defun backtrack (class &optional (bound-lower 1) (bound-upper 1) (state ""))
  (when (> bound-lower bound-upper)
    (error "bound-lower cannot exceed bound-upper"))              ;malformed expression
  (cond ((= (length state) bound-upper)                         ;base cases
         (return-from backtrack state))
        ((and (< (length state) bound-upper)
              (>= (length state) bound-lower))
         (when (= 0 (random (1+ (- bound-upper bound-lower))))  ;equal chance for all lengths between bounds
           (return-from backtrack state))))
  (backtrack class bound-lower bound-upper (concatenate 'string state (make-node class))))

